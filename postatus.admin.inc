<?php

/**
 * @file
 * Provides Po file analysis.
 */

/**
 * Hook menu callback.
 */
function postatus_admin_form($form_state) {
  // Get all languages, except English
  drupal_static_reset('language_list');
  $names = locale_language_list('name');
  unset($names['en']);

  if (!count($names)) {
    $languages = _locale_prepare_predefined_list();
    $default = key($languages);
  }
  else {
    $languages = array(
      t('Already added languages') => $names,
      t('Languages not yet added') => _locale_prepare_predefined_list()
    );
    $default = key($names);
  }

  $form['file'] = array('#type' => 'file',
    '#title' => t('Language file'),
    '#size' => 50,
    '#description' => t('A Gettext Portable Object (<em>.po</em>) file.'),
  );
  $form['langcode'] = array('#type' => 'select',
    '#title' => t('Language'),
    '#options' => $languages,
    '#default_value' => $default,
  );
  $form['group'] = array('#type' => 'radios',
    '#title' => t('Text group'),
    '#default_value' => 'default',
    '#options' => module_invoke_all('locale', 'groups'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Status'));

  if (!empty($_SESSION['postatus_batch_results'])) {
    $vars['header'] = array(
      'String',
      'File',
      'Drupal',
      'STATUS',
    );
    foreach ($_SESSION['postatus_batch_results']['strings'] as $key => $string) {
      $status = t('NOT IN DRUPAL');
      $class = 'error';
      if ($string['status'] == 1) {
        $status = t('IN DRUPAL BUT NO MATCH');
        $class = 'warning';
      }
      elseif ($string['status'] == 2) {
        $status = t('IN DRUPAL AND MATCH');
        $class = 'success';
      }      
      $vars['rows'][] = array(
        'data' => array(
          $key,
          $string['file'],
          $string['drupal'],
          $status,
        ),
        'class' => array($class),
      );
    }
    $form['table'] = array(
      '#theme' => 'table',
      '#header' => $vars['header'],
      '#rows' => $vars['rows'],
      '#caption' => $names[$_SESSION['locale_translation_filter']['language']],
    );
    $form['#attached']['css'] = array(
      drupal_get_path('module', 'postatus') . '/report.css',
    );
  }
  
  return $form;

}
/**
 * Start the batch process on submission
 */
function postatus_admin_form_submit(&$form, $form_state) {
  module_load_include('php', 'postatus', 'PoParser');  
  $parser = new Sepia\PoParser();
  $entries = $parser->parse($_FILES['files']['tmp_name']['file']);
  $total = count($entries);
  // Number of records to process per record.  
  $per_batch = 10;
  $batch_total = floor($total / $per_batch);
  $batch_total += ($total % $per_batch) > 0 ? 1 : 0;
  $counter = 0;
  $operations = array();
  $query = array(
    'translation' => 'all',
    'group' => $form_state['values']['group'],
    'language' => $form_state['values']['langcode'],
    'string' => '',
  ); 
  // Has to be set because drupal internals depend on session varaibles.
  $_SESSION['locale_translation_filter'] = $query;
  while ($counter < $batch_total) {
    $offset = $per_batch * $counter;
    $operations[] = array(
      'postatus_batch_process',
      array(array_slice($entries, $offset, $per_batch), $query),
    );
    $counter++;
  }
  $batch = array(
    'title' => t('Processing PO file'),
    'finished' => 'postatus_batch_finish',
    'operations' => $operations,
    'file' => drupal_get_path('module', 'postatus') . '/postatus.admin.inc',
  );
   batch_set($batch);
}

/**
 * Batch process to analyse uploaded po file.
 */
function postatus_batch_process($strings, $query, &$context) {
  $langcode = $_SESSION['locale_translation_filter']['language'];
  foreach ($strings as $string) {
    $msgid = current($string['msgid']);
    $msgstr = trim(current($string['msgstr']));
    $reference = trim(current($string['reference']));
    $query['string'] = $msgid;
    $translations = _postatus_locale_translate_seek($query);
    $context['results']['strings'][$msgid] = array(
      'status' => 0,
      'file' => $msgstr,
      'drupal' => '',
    );
    foreach ($translations as $translation) {
      if ($translation['languages'][$langcode] && $reference == $translation['location']) {
        $context['results']['strings'][$msgid]['status'] = $translation['languages'][$langcode] == $msgstr ? 2 : 1;
        $context['results']['strings'][$msgid]['drupal'] = $translation['languages'][$langcode];
        $context['results']['strings'][$msgid]['reference'] = $reference;
        $context['results']['strings'][$msgid]['context'] = $translation['location'];
        break;
      }
    }
  }
}

/**
 * Batch process finish callback.
 */
function postatus_batch_finish($success, $results, $operations) {
  $_SESSION['postatus_batch_results'] = $results;
}